function corrector()

verbose = false;

% some parameters
folder_imgs = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\';
folder_landmarks = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\landmarks\';
folder_corrected_landmarks = fullfile(folder_imgs, 'corrected-lm');
if(~exist(folder_corrected_landmarks, 'dir')), mkdir(folder_corrected_landmarks); end;
extension = '.jpg';

% list all 'extension' files in 'folder_imgs'
images = dir([folder_imgs '*' extension]);

% create figure and maximize
f = figure;

% gather all necessary data
data = guidata(f);
data.f = f;
data.img_to_correct = 1;
data.folder_imgs = folder_imgs;
data.folder_landmarks = folder_landmarks;
data.folder_corrected_landmarks = folder_corrected_landmarks;
data.extension = extension;
data.imgs = images;
data.n_imgs = numel(data.imgs);
guidata(f, data);
correct_landmarks(f);


% % ==================================================================>
% % FUNCTION THAT SHOW LANDMARKS AND ADD CALLBACK TO KEYPRESS
% % ==================================================================>
function correct_landmarks(f)

data = guidata(f);

% % Load image
img = imread(fullfile(data.folder_imgs, data.imgs(data.img_to_correct).name));

% % Load landmarks
lm_name = strrep(data.imgs(data.img_to_correct).name, data.extension, '.lm');
if(exist(fullfile(data.folder_corrected_landmarks, lm_name), 'file'))
    landmarks = load(fullfile(data.folder_corrected_landmarks, lm_name));
else
    landmarks = load(fullfile(data.folder_landmarks, lm_name));
end
n_lm = numel(landmarks(:,2));

% % place end contour landmarks at same height than eye's comisures and make contour landmarks equally spaced
landmarks = equiplaceLandmarks(landmarks);

% show image
figure(f);
hold off;
cla;
imshow(img);
ha = gca;

for nl=1:n_lm
    hp(nl) = impoint(ha, landmarks(nl, 1), landmarks(nl, 2));
end

set(f,'units','normalized','outerposition',[0 0 1 1]); % Maximize figure.

data.hp = hp;
data.point_to_correct = 1;
data.landmarks = landmarks;
data.n_lm = n_lm;
guidata(f, data);

title(num2str(data.img_to_correct));
% add callback <== THIS DOES THE WHOLE JOB
set(f, 'KeyPressFcn', @(f,keydata) keypress_function(f,keydata));
setColor(hp(1), 'green');

figure(gcf)
axes(gca) % get focus
drawnow

% % ==================================================================>
% % CALLBACK FOR KEYPRESS
% % ==================================================================>
function keypress_function(f,keydata)

data = guidata(f);
hp = data.hp;
ind = data.point_to_correct;

p = hp(ind);

try
    pos = getPosition(p);
catch
    data.point_to_correct = 1;
    data.img_to_correct = data.img_to_correct + 1;
    guidata(f, data);
    correct_landmarks(f);
end

switch(keydata.Key)
    case 'leftarrow'
        new_pos = [pos(1)-1 pos(2)];
    case 'rightarrow'
        new_pos = [pos(1)+1 pos(2)];
    case 'uparrow'
        new_pos = [pos(1) pos(2)-1];
    case 'downarrow'
        new_pos = [pos(1) pos(2)+1];
    case 'a'
        new_pos = [pos(1)-10 pos(2)];
    case 'd'
        new_pos = [pos(1)+10 pos(2)];
    case 'w'
        new_pos = [pos(1) pos(2)-10];
    case 's'
        new_pos = [pos(1) pos(2)+10];
        
    case 'backspace'    % % Go to previous landmark
        if(ind>1)
            data.point_to_correct = ind - 1;
            setColor(hp(ind), 'blue');
            setColor(hp(ind-1), 'green');
            guidata(f, data);
        end
        return;
        
    case 'return'   % % correct next landmark. if last, save all and next img
        % save current landmark's position
        data.landmarks(ind, :) = pos;
        % increment counter to modify next landmark
        data.point_to_correct = ind + 1;
        guidata(f, data);
        if(data.n_lm == ind)
            saveLandmarks(f);
            % Process next image
            if(data.img_to_correct < data.n_imgs)
                data.point_to_correct = 1;
                data.img_to_correct = data.img_to_correct + 1;
                guidata(f, data);
                correct_landmarks(f);
            else
                close(f);
                msgbox('All images corrected.');
            end
        else
            setColor(hp(ind), 'blue');
            setColor(hp(ind+1), 'green');
        end
        return;
        
    case 'pageup'   % % Go to previous image (without saving)
        if(data.img_to_correct > 1)
            data.img_to_correct = data.img_to_correct - 1;
            guidata(f, data);
            correct_landmarks(f);
            return;
        end
        return; % if first image reached just return
        
    case 'pagedown' % % Go to next image (without saving)
        if(data.img_to_correct < data.n_imgs)
            data.img_to_correct = data.img_to_correct + 1;
            guidata(f, data);
            correct_landmarks(f);
        else
            msgbox('All images corrected.');
        end
        return;
        
    case 'space' % % Save landmarks and go to next image
        % save current landmark's position
        data.landmarks(ind, :) = pos;
        guidata(f, data);
        saveLandmarks(f);
        % Process next image
        if(data.img_to_correct < data.n_imgs)
            data.point_to_correct = 1;
            data.img_to_correct = data.img_to_correct + 1;
            guidata(f, data);
            correct_landmarks(f);
        else
            close(f);
            msgbox('All images corrected.');
        end
        return;
        
    case 'g' % go to image_to_correct #
        img_num = str2double(inputdlg('IMG to correct #: '));
        if(img_num > 0 && img_num <= data.n_imgs)
            data.img_to_correct = img_num;
            data.point_to_correct = 1;
            guidata(f, data);
            correct_landmarks(f);
        else
            msgbox(['Number of images: ' num2str(data.n_imgs)]);
        end
        return;
        
    otherwise
        return;
end
setPosition(p, new_pos);

% % ==================================================================>
% % FUNCTION THAT SAVE LANDMARKS
% % ==================================================================>
function saveLandmarks(f)

data = guidata(f);

% % Save corrected landmarks in faces and its locations
fp = fopen(fullfile(data.folder_corrected_landmarks, strrep(data.imgs(data.img_to_correct).name, data.extension, '.lm')), 'w');
for nl=1:size(data.landmarks, 1)
    fprintf(fp,'%5f %5f\n',data.landmarks(nl, :));
end
fclose(fp);
% delete impoints and plot *
for nl=1:data.n_lm
    delete(data.hp(nl));
end
hold on, plot(data.landmarks(:,1), data.landmarks(:,2), 'r.');
saveas(f, [data.folder_corrected_landmarks, '\', strrep(data.imgs(data.img_to_correct).name, data.extension, ['_lm.bmp'])]);
% close(data.fig); % DON'T DESTROY, NEED GUIDATA TO SAVE DATA

% % ==================================================================>
% % FUNCTION THAT PLACE LANDMARKS AT EYE'S HEIGHT
% % ==================================================================>
function landmarks = equiplaceLandmarks(landmarks)

landmarks(1,2) = landmarks(37,2);
landmarks(17,2) = landmarks(46,2);
